FROM jetty:9.4.12-alpine

WORKDIR /var/lib/jetty/
CMD ["java","-jar","/usr/local/jetty/start.jar"]
EXPOSE 8080
